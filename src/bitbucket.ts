import * as execa from 'execa';
import * as vscode from 'vscode';
import { URL } from 'url';
import urljoin = require('url-join');
import gitUrlParse = require('git-url-parse');


const bitbucketURL = "https://bitbucket.org";
const extensionName = "bitbucket-pull-requests";

export class Bitbucket{
    private root: string;

    constructor(root: string) {
        this.root = root;
    }

    private static getActiveWorkspaceFolder(): vscode.Uri | undefined {
        if (!vscode.workspace.workspaceFolders) {
            // no workspace open
            return undefined;
        }
        if (vscode.workspace.workspaceFolders.length === 1) {
            // just one workspace open
            return vscode.workspace.workspaceFolders[0].uri;
        }
        // check which workspace status should be visible
        const editor = vscode.window.activeTextEditor;
        if (!editor) {
            return undefined;
        }
        const folder = vscode.workspace.getWorkspaceFolder(editor.document.uri);
        if (!folder) {
            return undefined;
        }
        return folder.uri;
    }

    public static async SaveConfiguredURL(url: string) : Promise<void> {
        const uri = this.getActiveWorkspaceFolder();
        const workspaceConfiguration = vscode.workspace.getConfiguration(undefined, uri);
        workspaceConfiguration.update(`${extensionName}.serverURL`, url, false);
        return;
    }

    static getConfiguredURL() : string {
        const uri = this.getActiveWorkspaceFolder();
        if (!uri){
            return bitbucketURL;
        }

        const configuration = vscode.workspace.getConfiguration(undefined, uri).get<string>(`${extensionName}.serverURL`);
        if (!configuration){
            return bitbucketURL;
        }

        const url = configuration || bitbucketURL;
        return url;
    }

    public static async GetBitbucketURL() : Promise<string> {
        
        const placeHolder = this.getConfiguredURL();
        const urlInput = await vscode.window.showInputBox({
            ignoreFocusOut: true,
            placeHolder: placeHolder
          });
       if (urlInput){
           try{
            const u = new URL(urlInput);
            return u.toString();
           }catch(e) {
            throw new Error(`'${urlInput}' is not a valid URL`);
           }
       }

       return "";
    }

    public async GetPullRequestURL(): Promise<string> {
        const isAvailable = await this.checkExistence();
        if (!isAvailable) {
            throw new Error(`No git executable found. Please install git`);
        }
        const {branch, remote} = await this.getBranchAndRemote();
        const {owner, repo} = await this.getOwnerAndRepo(remote);
        const url = Bitbucket.getConfiguredURL();
        const encodedBranch = encodeURIComponent(branch);
        
        return buildPullRequestURL(url, owner, repo, encodedBranch);
    }

    private async getBranchAndRemote() : Promise<{branch: string, remote: string}>{
        try{
            const {stdout} = await this.execute(`git rev-parse --abbrev-ref --symbolic-full-name @{u}`);
            console.log(`branch/remote:  ${stdout.trim()}`);
            const index = stdout.trim().indexOf("/");
            const remote = stdout.substr(0, index);
            const branch = stdout.substr(index + 1);
            return {branch: branch, remote: remote};
        }catch(e){
            if (e.message.includes("Not a git repository")) {
                throw new Error(`Your current workspace is not a git repository. If your workspace contains more than one repo, call the command from an opened file.`);
            }
            else if (e.message.includes("no upstream configured for branch")) {
                throw new Error(`This branch doesn't have a remote configured.`);
            }

            throw e;
        }
    }

    private async getOwnerAndRepo(remote: string) : Promise<{owner: string, repo: string}> {
        const {stdout} = await this.execute(`git remote get-url ${remote}`);
        console.log(`remote url:  ${stdout.trim()}`);
        const parsed = gitUrlParse(stdout.trim());
        return {owner: parsed.owner, repo: parsed.name};
    }

    private async execute(cmd: string) : Promise<{stdout: string, stderr: string}>{
        const [git, ...args] = cmd.split(' ');
        return execa(git || git, args, {cwd: this.root});
    }

    private async checkExistence(): Promise<boolean> {
        try {
          await this.execute('git --version');
          return true;
        } catch (e) {
          return false;
        }
      }
}

export function buildPullRequestURL(url: string, owner: string, repo: string, branch: string): string {
    if (url === bitbucketURL){
        return urljoin(url, `${owner}/${repo}/pull-requests/new?source=${branch}&t=1`);
    } else{
        // bitbucket server
        // This is to work around https://github.com/IonicaBizau/git-url-parse/issues/97
        const o = owner.replace('scm/', '');
        return urljoin(url, `projects/${o}/repos/${repo}/pull-requests?create&sourceBranch=${branch}&t=1`);
    }
}