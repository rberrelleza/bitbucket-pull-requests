//
// Note: This example test is leveraging the Mocha test framework.
// Please refer to their documentation on https://mochajs.org/ for help.
//

// The module 'assert' provides assertion methods from node
import * as assert from 'assert';
import * as bitbucket from '../bitbucket';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
// import * as vscode from 'vscode';
// import * as myExtension from '../extension';

// Defines a Mocha test suite to group tests of similar kind together
suite("Extension Tests", function () {

    // Defines a Mocha unit test
    test("Something 1", function() {
        assert.equal(-1, [1, 2, 3].indexOf(5));
        assert.equal(-1, [1, 2, 3].indexOf(0));
    });

    test("buildPullRequestURL", function(){
        assert.equal(
            bitbucket.buildPullRequestURL("https://bitbucket.org", "ramiroberrelleza", "bitbucket-pull-requests", "test"),
            `https://bitbucket.org/ramiroberrelleza/bitbucket-pull-requests/pull-requests/new?source=test&t=1`
        );

        assert.equal(
            bitbucket.buildPullRequestURL("https://bitbucket.org", "ramiroberrelleza", "bitbucket-pull-requests", "test"),
            `https://bitbucket.org/ramiroberrelleza/bitbucket-pull-requests/pull-requests/new?source=test&t=1`
        );

        assert.equal(
            bitbucket.buildPullRequestURL("https://bitbucket-server", "ramiroberrelleza", "bitbucket-pull-requests", "test"),
            `https://bitbucket-server/projects/ramiroberrelleza/repos/bitbucket-pull-requests/pull-requests?create&sourceBranch=test&t=1`
        );
    });
});