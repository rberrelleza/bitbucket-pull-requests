'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as bitbucket from './bitbucket';
import * as platform from './platform';
import * as cp from 'child_process';
import { dirname } from 'path';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    console.log('Congratulations, your extension "bitbucket-pull-requests" is now active!');

    let createPullRequest = vscode.commands.registerCommand('extension.createPullRequest', () => {

        let repoPath;
        const currentEditor = vscode.window.activeTextEditor;
        if (currentEditor) {
            repoPath = dirname(currentEditor.document.uri.fsPath);
        } else{
            const folders = vscode.workspace.workspaceFolders;
            if (!folders) {
                vscode.window.showErrorMessage(`This extension only works within git folders.`);
                return;
            }

            repoPath = folders[0].uri.fsPath;
        }
            
        const bb = new bitbucket.Bitbucket(repoPath);
        const result = bb.GetPullRequestURL();
        result.then((url) => {
            const command = platform.getRunCommand(url);    
            console.log(`running ${command}`);
            cp.exec(`${command}`).on('error', (err: Error) => {
                vscode.window.showErrorMessage(`Couldn't open a browser window. Make sure you have a browser installed. ${err}`);
            });
        }).catch((reason) => {
            vscode.window.showErrorMessage(`Couldn't start your pr: ${reason}`);
        });
    });
    context.subscriptions.push(createPullRequest);

    let setBitbucketURL = vscode.commands.registerCommand('extension.setBitbucketURL', () => {
        bitbucket.Bitbucket.GetBitbucketURL().then((url) => {
            if (url) {
                bitbucket.Bitbucket.SaveConfiguredURL(url)
                .then (()=>{
                    vscode.window.showInformationMessage("Bitbucket Server URL configured");
                })
                .catch((reason) => {
                    vscode.window.showErrorMessage(reason);
                });
            }
        }).catch((reason) => {
            vscode.window.showErrorMessage(reason);
        });
    });
    context.subscriptions.push(setBitbucketURL);
}

// this method is called when your extension is deactivated
export function deactivate() {
}

