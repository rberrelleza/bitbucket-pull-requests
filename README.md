# Bitbucket Pull Requests

This extension adds a command that lets you start a pull request from the current branch into your repository's default branch. 

## Usage
Use the command palette (⇧⌘P) to start a pull request by selecting the "Bitbucket: Create pull request from current branch" command.

![create a pr from vscode](https://bitbucket.org/rberrelleza/bitbucket-pull-requests/raw/master/assets/command.png)


### Bitbucket server
Use the command palette (⇧⌘P) to configure your Bitbucket server a pull request by selecting the "Bitbucket: Set the Bitbucket Server URL" command.

![set the URL](https://bitbucket.org/rberrelleza/bitbucket-pull-requests/raw/master/assets/serverURLcommand.png)


You can also update your workspace settings directly via the configuration UI:

![set the URL via the settings](https://bitbucket.org/rberrelleza/bitbucket-pull-requests/raw/master/assets/settings.png)

## Requirements
You must have `git` installed and it has to be on the `$PATH`

Your default browser must be able to reach `https://bitbucket.org` or your bitbucket server instance.

## Known Issues

If a browser is not available, the extension won't be able to start the pull request.

Please report any issues [here](https://bitbucket.org/rberrelleza/bitbucket-pull-requests/issues).