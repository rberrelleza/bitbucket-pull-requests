# Change Log
All notable changes to the "bitbucket-pull-requests" extension will be documented in this file.

## Release Notes

### 0.3.11 - 2019-09-27
Remote extra slashes on URL

### 0.3.10 - 2019-09-27
Fix escaping issue in Windows

### 0.3.9 - 2019-03-06
Remove `scm/` from Bitbucket Server URLs

### 0.3.5 - 2019-02-23
Correctly URL escape the branch name.

### 0.3.4 - 2019-02-01
Fix #5, calculate the right source branch when the branch has slashes in the name.

### 0.3.3 - 2019-01-26
Fix #4, use `start` instead of `explorer` to launch the browser on windows machines.

### 0.3.1 - 2019-01-11
Use a specific command per OS to open the browser

### 0.3.0 - 2018-11-02
Support for Bitbucket server

### 0.2.1 - 2018-10-31
Autodetect the repo based on the open file

### 0.2.0 - 2018-10-21
Fix error message when there's no branch

## [0.1.1] - 2018-10-21
- Fix publishing metadata

## [0.1.0] - 2018-10-21
- Initial release



